#!/bin/bash
sudo rm  /home/ec2-user/output.txt
curl -s -d @request.json --header "Content-Type: application/json" http://localhost:8000/feed/status/ >> /home/ec2-user/output.txt
foo=$(awk '{ print $4}' /home/ec2-user/output.txt)
echo $foo
bar='Completed'
id=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
echo $bar
echo $id
if [[ $foo == $bar ]]
then
  aws lambda invoke --invocation-type RequestResponse --function-name [InstanceCreatorLambdaFunctionName] --region [Region] --log-type Tail --payload '{  "instance_id": "'$id'" }' outputfile.txt
fi
